@file:Suppress("unused") // usages in build scripts are not tracked properly

import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.UnknownDomainObjectException
import org.gradle.api.artifacts.ConfigurablePublishArtifact
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.file.DuplicatesStrategy
import org.gradle.api.internal.artifacts.publish.ArchivePublishArtifact
import org.gradle.api.plugins.BasePluginConvention
import org.gradle.api.tasks.AbstractCopyTask
import org.gradle.api.tasks.Copy
import org.gradle.api.tasks.Upload
import org.gradle.api.tasks.javadoc.Javadoc
import org.gradle.jvm.tasks.Jar
import java.io.File


private const val MAGIC_DO_NOT_CHANGE_TEST_JAR_TASK_NAME = "testJar"

fun Project.testsJar(body: Jar.() -> Unit = {}): Jar {
    val testsJarCfg = getConfigurations().getOrCreate("tests-jar").extendsFrom(getConfigurations().getAt("testCompile"))

    return getTasks().create(MAGIC_DO_NOT_CHANGE_TEST_JAR_TASK_NAME,Jar::class.java) {
        dependsOn("testClasses")
        pluginManager.withPlugin("java") {
            from(testSourceSet.output)
        }
        classifier = "tests"
        body()
        project.addArtifact(testsJarCfg, this, this)
    }
}

fun customTestsJar(project:Project,body: Jar.() -> Unit = {}):Jar{return project.testsJar(body)}

var Project.artifactsRemovedDiagnosticFlag: Boolean
    get() = getExtensions().getExtraProperties().has("artifactsRemovedDiagnosticFlag") && getExtensions().getExtraProperties()["artifactsRemovedDiagnosticFlag"] == true
    set(value) {
        getExtensions().getExtraProperties().set("artifactsRemovedDiagnosticFlag", value)
    }

fun Project.removeArtifacts(configuration: Configuration, task: Task) {
    configuration.artifacts.removeAll { artifact ->
        artifact.file in task.outputs.files
    }

    artifactsRemovedDiagnosticFlag = true
}

fun Project.noDefaultJar() {
    tasks.findByName("jar")?.let { defaultJarTask ->
        defaultJarTask.enabled = false
        defaultJarTask.actions = emptyList()
        configurations.forEach { cfg ->
            removeArtifacts(cfg, defaultJarTask)
        }
    }
}

fun customNoDefaultJar(project:Project){return project.noDefaultJar()}

fun Project.runtimeJarArtifactBy(task: Task, artifactRef: Any, body: ConfigurablePublishArtifact.() -> Unit = {}) {
    addArtifact("archives", task, artifactRef, body)
    addArtifact("runtimeJar", task, artifactRef, body)
    configurations.findByName("runtime")?.let {
        addArtifact(it, task, artifactRef, body)
    }
}

fun customRuntimeJarArtifactBy(project:Project,task: Task, artifactRef: Any, body: ConfigurablePublishArtifact.() -> Unit = {}){
	return project.runtimeJarArtifactBy(task,artifactRef,body)
}

fun <T : Jar> Project.runtimeJar(task: T, body: T.() -> Unit = {}): T {
    getExtensions().getExtraProperties().set("runtimeJarTask", task)
    tasks.findByName("jar")?.let { defaultJarTask ->
        removeArtifacts(configurations.getOrCreate("archives"), defaultJarTask)
    }
    return task.apply {
        setupPublicJar(project.getConvention().getPlugin(BasePluginConvention::class.java).archivesBaseName)
        setDuplicatesStrategy(DuplicatesStrategy.EXCLUDE)
        body()
        project.runtimeJarArtifactBy(this, this)
    }
}

fun <T : Jar> customRuntimeJar(project:Project,task: T, body: T.() -> Unit = {}): T {return project.runtimeJar(task,body)}

fun Project.runtimeJar(body: Jar.() -> Unit = {}): Jar = runtimeJar(getOrCreateTask("jar", body), { })

fun customRuntimeJar(project : Project,body: Jar.() -> Unit = {}):Jar{return project.runtimeJar(body)}

fun customRuntimeJar(project : Project,choice:String): Jar {
	when(choice){
		"default"->{
			return project.runtimeJar({})
		}
		"android-lint"->{ 
			return project.runtimeJar({
    			fromEmbeddedComponents()
				})
		}
		else -> {println("You messed up somewhere grep \"CONFRUNTIMEJARTASK\" to come here and see options")
				return project.runtimeJar({})
		}
	}
}

fun Project.sourcesJar(sourceSet: String? = "main", body: Jar.() -> Unit = {}): Jar =
    getOrCreateTask("sourcesJar") {
        setDuplicatesStrategy(DuplicatesStrategy.EXCLUDE)
        classifier = "sources"
        try {
            if (sourceSet != null) {
                project.getPluginManager().withPlugin("java-base") {
                    from(project.javaPluginConvention().getSourceSets().getAt(sourceSet).getAllSource())
                }
            }
        } catch (e: UnknownDomainObjectException) {
            // skip default sources location
        }
        body()
        project.addArtifact("archives", this, this)
    }
fun customSourcesJar(project:Project,sourceSet: String? = "main", body: Jar.() -> Unit = {}): Jar{return project.sourcesJar(sourceSet,body)}
fun Project.javadocJar(body: Jar.() -> Unit = {}): Jar = getOrCreateTask("javadocJar") {
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    classifier = "javadoc"
    tasks.findByName("javadoc")?.let { it as Javadoc }?.takeIf { it.enabled }?.let {
        dependsOn(it)
        from(it.destinationDir)
    }
    body()
    project.addArtifact("archives", this, this)
}


fun Project.standardPublicJars() {
    runtimeJar()
    sourcesJar()
    javadocJar()
}

fun Project.publish(body: Upload.() -> Unit = {}): Upload {
    //apply<plugins.PublishedKotlinModule>()

    if (artifactsRemovedDiagnosticFlag) {
        error("`publish()` should be called before removing artifacts typically done in `noDefaultJar()` of `runtimeJar()` calls")
    }

    afterEvaluate {
        if (configurations.findByName("classes-dirs") != null)
            throw GradleException("classesDirsArtifact() is incompatible with publish(), see sources comments for details")
    }

    return (tasks.getByName("uploadArchives") as Upload).apply {
        body()
    }
}

fun Project.ideaPlugin(subdir: String = "lib", body: AbstractCopyTask.() -> Unit): Copy {
    val thisProject = this
    val pluginTask = getTasks().create("ideaPlugin",Copy::class.java) {
        body()
        into(File(rootProject.getExtensions().getExtraProperties().get("ideaPluginDir").toString(), subdir).path)
        rename("-${java.util.regex.Pattern.quote(thisProject.version.toString())}", "")
    }

    getTasks().create("idea-plugin") {
        dependsOn(pluginTask)
    }

    return pluginTask
}
fun customIdeaPlugin(project:Project,subdir: String = "lib", body: AbstractCopyTask.() -> Unit): Copy {return project.ideaPlugin(subdir,body)}
fun customIdeaPlugin(project : Project, subdir: String = "lib", jar:Task ,choice:String): Copy {
	when(choice){
		"allopen-cli"->{
			return project.ideaPlugin {
					from(jar)
				}
		}
		else->{
			return project.ideaPlugin {
					from(jar)
				}
		}
	}


}

fun Project.ideaPlugin(subdir: String = "lib"): Copy = ideaPlugin(subdir) {
    runtimeJarTaskIfExists()?.let {
        from(it)
    }
}

fun customIdeaPlugin(project: Project): Copy {return project.ideaPlugin()}

fun Project.dist(
    targetDir: File? = null,
    targetName: String? = null,
    fromTask: Task? = null,
    body: AbstractCopyTask.() -> Unit = {}
): AbstractCopyTask {
    val distJarCfg = configurations.getOrCreate("distJar")
    val distLibDir: File = rootProject.getExtensions().getExtraProperties().get("distLibDir") as File
    val distJarName = targetName ?: (getConvention().getPlugin(BasePluginConvention::class.java).archivesBaseName + ".jar")
    val thisProject = this

    return getTasks().create("dist",Copy::class.java) {
        body()
        (fromTask ?: runtimeJarTaskIfExists())?.let {
            from(it)
            if (targetName != null) {
                rename(it.outputs.files.singleFile.name, targetName)
            }
        }
        rename("-${java.util.regex.Pattern.quote(thisProject.version.toString())}", "")
        into(targetDir ?: distLibDir)
        project.addArtifact(distJarCfg, this, File(targetDir ?: distLibDir, distJarName))
    }
}

fun customDist(
	project:Project,
    targetDir: File? = null,
    targetName: String? = null,
    fromTask: Task? = null,
    body: AbstractCopyTask.() -> Unit = {}
): AbstractCopyTask {return project.dist(targetDir,targetName,fromTask,body)}

private fun Project.runtimeJarTaskIfExists(): Task? =
    if (getExtensions().getExtraProperties().has("runtimeJarTask")) getExtensions().getExtraProperties().get("runtimeJarTask") as Task
    else tasks.findByName("jar")


fun ConfigurationContainer.getOrCreate(name: String): Configuration = findByName(name) ?: create(name)

fun Jar.setupPublicJar(baseName: String, classifier: String = "") {
    val buildNumber = project.rootProject.getExtensions().getExtraProperties().get("buildNumber") as String
    this.baseName = baseName
    this.classifier = classifier
    manifest.attributes.apply {
        put("Implementation-Vendor", "JetBrains")
        put("Implementation-Title", baseName)
        put("Implementation-Version", buildNumber)
    }
}

fun customSetupPublicJar(jar: Jar,baseName: String, classifier: String = "") {return jar.setupPublicJar(baseName,classifier)}

fun Project.addArtifact(configuration: Configuration, task: Task, artifactRef: Any, body: ConfigurablePublishArtifact.() -> Unit = {}) {
    artifacts.add(configuration.name, artifactRef) {
        builtBy(task)
        body()
    }
}

fun Project.addArtifact(configurationName: String, task: Task, artifactRef: Any, body: ConfigurablePublishArtifact.() -> Unit = {}) =
    addArtifact(configurations.getOrCreate(configurationName), task, artifactRef, body)

fun Project.cleanArtifacts() {
    getConfigurations().getAt("archives").artifacts.let { artifacts ->
        artifacts.forEach {
            artifacts.remove(it)
        }
    }
}
